# Arduino Make file. Refer to https://github.com/sudar/Arduino-Makefile

BOARD_TAG    	=  nano328

USER_LIB_PATH	= ../libraries
ARDUINO_LIBS	= TimerOne Melody AfficheurLed AnalogRead Bouton
MONITOR_PORT  = /dev/ttyUSB0
# uncomment next line for debug on serial monitor
#CPPFLAGS		= -DDEBUG

include /usr/share/arduino/Arduino.mk
