/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _Minuteur_H_
#define _Minuteur_H_
#include "Arduino.h"
#include <Melody.h> // https://gitlab.com/VinceLim/Melody/tags/v0.0.1

#define VERSION "1.0.1"

// Pin def, cf Minuteur.fzz
#define LED_RED A0
#define LED_ORANGE A1
#define LED_GREEN A2
#define BUTTON_START 2
#define POT_SECOND_READ A6
#define POT_MINUTE_READ A7
#define PIN_BUZZER 11
#define PIN_READ_BATT A4

// LED-displayMapping (LTC-4627JS)
#define DIG_1 A5
#define DIG_2 A3
#define DIG_3 3
#define DIG_4 4
#define DIG_DP 13
#define SEG_A 5
#define SEG_B 6
#define SEG_C 7
#define SEG_D 8
#define SEG_E 9
#define SEG_F 10
#define SEG_G 12


// constants
#define RINGING_TIME 2000

// Batterie (lecture pile/2 * 10)
#define BATT_FAIBLE 42
#define BATT_TRES_FAIBLE 37



#ifdef DEBUG
#define init_debug() Serial.begin(9600)
#define debug(s) Serial.print(s)
#define debugln(s) Serial.println(s)
#else
#define init_debug() //Serial.begin(9600)
#define debug(s) //Serial.print(s)
#define debugln(s) //Serial.println(s)
#endif //DEBUG


typedef enum _MinuteurState {IDLE, RUNNING, RINGING, SLEEP} MinuteurState;

bool inline running(MinuteurState etat);
bool inline ringing(MinuteurState);
bool inline idle(MinuteurState);

int startCounting(int minute, int seconde);

void displayState();
int checkEnd();

/**
 * Vert : Idle
 * Vert + Orange : batterie faible <8V
 * Vert + Rouge : batterie très faible <7.4V
 * Orange : En route
 * Rouge : sonnerie
 */
void displayLed();

void displayTime();

int displayRemainingTime();
int displaySelectedTime(int,int);

void playSound();

unsigned long getRemaining();
void goToSleep();
int displayBat();
int markChange();
void testSleep();
void changeState(MinuteurState);
void recordLastRead(int, int);
void pin2Interrupt(void);
Melody chooseMusic(Melody* melodies, int* weights, int tab_size);
#endif /* _Minuteur_H_ */
