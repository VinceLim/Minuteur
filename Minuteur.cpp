/*
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Minuteur.h"
#include <Bouton.h> // https://gitlab.com/VinceLim/Bouton/tags/v0.0.1
#include <AfficheurLed.h> // https://gitlab.com/VinceLim/AfficheurLed/tags/v0.0.2
#include <AnalogRead.h> // https://gitlab.com/VinceLim/AnalogRead/tags/v0.0.1

#include <avr/sleep.h>
#include <avr/power.h>

/*
 Minuteur Sketch, test board
 */

// Global var :
unsigned long endChrono = 0;
unsigned long endRinging = 0;
MinuteurState state = IDLE;

int lastMin = 0;
int lastSec = 0;

unsigned long lastChange = 0;
const unsigned long SLEEP_TIME = 10000;

Bouton startButton(BUTTON_START, UP);
AnalogRead minutePot(POT_MINUTE_READ, 0, 101, 5, 50);
AnalogRead secondePot(POT_SECOND_READ, 0, 61, 5, 50);
// Convertit la lecture 0V-5V sur PIN_READ_BATT en nombre de 0 à 50
AnalogRead batterieTest(PIN_READ_BATT, 0, 49, 1, 1000);
Player player(PIN_BUZZER);

// Quelques melodies
unsigned int sncf_notes[] = { NOTE_C4, NOTE_G4, NOTE_GS4, NOTE_DS4 };
long sncf_tempo[] = { 4, 4, -3, 2 };
Melody sncf(sncf_notes, sncf_tempo, 4, 150);

unsigned int mars_notes[] = { NOTE_D4, NOTE_D4, NOTE_D4, NOTE_G4, NOTE_G4,
NOTE_A4, NOTE_A4, NOTE_D5, NOTE_B4, NOTE_G4 };
long mars_tempo[] = { 1, 3, 1, 4, 4, 4, 4, 6, 2, 2 };
Melody mars(mars_notes, mars_tempo, 10, 150);

unsigned int bigben_notes[] = {NOTE_GS4, NOTE_E4, NOTE_FS4, NOTE_B3, NOTE_NONE, NOTE_B3, NOTE_FS4,NOTE_GS4, NOTE_E4};
long bigben_tempo[] = {4,4,4,8,4,4,4,4,8};
Melody bigben(bigben_notes, bigben_tempo,9,150);

unsigned int inter_notes[] = {NOTE_E5, NOTE_D5, NOTE_C5, NOTE_G4, NOTE_A4, NOTE_A4, NOTE_F4};
long inter_tempo[] = {3,1,8,6,2,-8,4};
Melody inter(inter_notes, inter_tempo, 7, 150);

unsigned int impmarch_note[] = {NOTE_G4, NOTE_G4, NOTE_G4, NOTE_DS4, NOTE_AS4,/*1*/
		NOTE_G4, NOTE_DS4, NOTE_AS4,NOTE_G4};
long impmarch_tempo[] = {4,4,4,3,1,/*1*/
		4,3,1,8};
// Noire = 4, 600ms
Melody impmarch(impmarch_note, impmarch_tempo, 9, 150);

// Noire = 12, 600ms
unsigned int sw_notes[] = {NOTE_G3,NOTE_G3,NOTE_G3 /*1*/,
NOTE_C4,NOTE_G4 /*2*/,
NOTE_F4, NOTE_E4, NOTE_D4, NOTE_C5, NOTE_G4,/*3*/
NOTE_F4, NOTE_E4, NOTE_D4, NOTE_C5, NOTE_G4,/*4*/
NOTE_F4, NOTE_E4, NOTE_F4,NOTE_D4};
long sw_tempo[]={4,4,4 /*1*/,
24, 24, /*2*/
4,4,4,24,12,/*3*/
4,4,4,24,12,/*4*/
4,4,4,24};
Melody sw(sw_notes,sw_tempo, 19, 50);

Melody playlist[] = {sncf, mars, bigben, inter, impmarch, sw};
int random_w[] = {4,1,4,1,4,4};
const int playlist_size=6;



void setup() {
	init_debug();

	// initialize pins
	pinMode(LED_RED, OUTPUT);
	pinMode(LED_ORANGE, OUTPUT);
	pinMode(LED_GREEN, OUTPUT);

	digitalWrite(LED_RED, HIGH);
	delay(200);
	digitalWrite(LED_ORANGE, HIGH);
	delay(200);
	digitalWrite(LED_GREEN, HIGH);
	delay(200);

	digitalWrite(LED_RED, LOW);
	digitalWrite(LED_ORANGE, LOW);
	digitalWrite(LED_GREEN, LOW);
	startButton.init();
	afficheur.init(DIG_1, DIG_2, DIG_3, DIG_4, DIG_DP, SEG_A, SEG_B, SEG_C,
	SEG_D,
	SEG_E, SEG_F, SEG_G);
	afficheur.testSequence();
	debug("Version:");
	debugln(VERSION);
	debugln("Ready");
	afficheur.afficherDeuxPoints(true);
	afficheur.allumer(true);
	player.play(NOTE_A6, 200);
	changeState(IDLE);

}

// the loop function runs over and over again forever
void loop() {

	// Wait button press
	if (startButton.readDebounceButton()) {
		int started=0;
		if (!running(state)) {
			started = startCounting(minutePot.getValue(), secondePot.getValue());
		}
		if(!started){
			player.play(NOTE_C3, 200);
		}
	}

	testSleep();

	displayState();

	checkEnd();

}

int checkEnd() {
	if ((running(state)) && millis() > endChrono) {
		debugln("END CHRONO");
		debug("millis : ");
		debugln(millis());
		debug("endChrono : ");
		debugln(endChrono);
		changeState(RINGING);
		endRinging = millis() + RINGING_TIME;
	}
	if ((ringing(state)) && millis() > endRinging) {
		debugln("END RINGING");
		afficheur.afficherDeuxPoints(true);
		changeState(IDLE);
	}

	return state;
}

void displayState() {

	displayLed();

	displayTime();

	playSound();

}

int startCounting(int min, int sec) {
	unsigned long duration = (unsigned long) sec * 1000UL
			+ (unsigned long) min * 60000UL;
	if(duration==0){
		// Pas de temps donné
		return 0;
	}
	debugln("START CHRONO");
	debug(min);
	debug(":");
	debugln(sec);
	changeState(RUNNING);
	endChrono = millis() + duration;
	debug("endChrono : ");
	debugln(endChrono);
	return endChrono;
}

void displayLed() {
	digitalWrite(LED_GREEN, LOW);
	digitalWrite(LED_ORANGE, LOW);
	digitalWrite(LED_RED, LOW);
	int batt;
	switch (state) {
	case IDLE:
		digitalWrite(LED_GREEN, HIGH);
		batt = batterieTest.getValue();
		if (batt < BATT_TRES_FAIBLE) {
			digitalWrite(LED_RED, HIGH);
		} else if (batt < BATT_FAIBLE) {
			digitalWrite(LED_ORANGE, HIGH);
		}
		break;
	case RUNNING:
		// Clignotement vert/orange
		{
			unsigned long remaining = endChrono - millis();
			if((remaining/500)%2){
				digitalWrite(LED_ORANGE, HIGH);
			} else {
				digitalWrite(LED_GREEN, HIGH);
			}
		}
		break;
	case RINGING:
		digitalWrite(LED_RED, HIGH);
		break;
	case SLEEP:
		digitalWrite(LED_ORANGE, HIGH);
		break;
	}

}

void displayTime() {
	if (running(state)) {
		displayRemainingTime();
	} else if (idle(state)) {

		int min = minutePot.getValue();
		int sec = secondePot.getValue();
		recordLastRead(min, sec);

		if (min > 0 || sec > 0) {
			displaySelectedTime(min, sec);
		} else {
			displayBat();
		}
	} else if (ringing(state)) {
		uint8_t tirets[] = { _segG, _segG, _segG, _segG };
		afficheur.rawInput(tirets, 4);
		// TODO : ne pas faire ça à chaque fois, associer au changement d'état.
	}
}

void playSound() {
	if (ringing(state)) {
		// Désactivation de l'affichage avant
		afficheur.allumer(false);
		Melody music = chooseMusic(playlist, random_w, playlist_size);
		player.play(music);
		while (player.isPlaying()) {
			//debug(".");
			// attente fin musique ?
		}

		afficheur.afficherDeuxPoints(true);
		afficheur.allumer(true);
		changeState(IDLE);

	}
}

bool running(MinuteurState etat) {
	return etat == RUNNING;
}

bool ringing(MinuteurState etat) {
	return etat == RINGING;
}

bool idle(MinuteurState etat) {
	return etat == IDLE;
}

bool isAsleep(MinuteurState etat){
	return etat==SLEEP;
}

int displayRemainingTime() {

	unsigned long r_time = getRemaining();
	int sec = r_time % 60;
	int min = r_time / 60;
	char stime[5];
	snprintf(stime, 5, "%02d%02d", min, sec);
	afficheur.mapAffichage(stime);
	return 0;
}

int displaySelectedTime(int min, int sec) {

	char stime[5];
	snprintf(stime, 5, "%02d%02d", minutePot.getValue(), secondePot.getValue());
	afficheur.afficherDeuxPoints(true);
	afficheur.mapAffichage(stime);
	return 0;

}

int displayBat() {
	int bat = 2 * batterieTest.getValue();
	char sbat[5];
	snprintf(sbat, 5, " %02dv", bat);
	afficheur.afficherDeuxPoints(true);
	afficheur.mapAffichage(sbat);
	return bat;
}

/**
 * en secondes
 */
unsigned long getRemaining() {
	return (endChrono - millis()) / 1000;
}

void testSleep() {
	unsigned long time = millis();

	if (idle(state) && lastChange + SLEEP_TIME < time) {
		goToSleep();
	}
}

int markChange() {
	lastChange = millis();
	return lastChange;
}

void goToSleep() {
	debugln("Sleep");
	afficheur.allumer(false);
	changeState(SLEEP);
	displayLed();
	/* Setup pin2 as an interrupt and attach handler. */
	attachInterrupt(0, pin2Interrupt, LOW);
	delay(100);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable()
	;
	sleep_mode()
	;
	/** The program will continue from here. **/
	/* First thing to do is disable sleep. */
	sleep_disable()
	;

	// Wake up

	debugln("Wake");
	afficheur.allumer(true);
	afficheur.afficherDeuxPoints(false);
	afficheur.mapAffichage("----");
	delay(100);
	afficheur.mapAffichage(" ---");
	delay(100);
	afficheur.mapAffichage("  --");
	delay(100);
	afficheur.mapAffichage("   -");
	delay(100);
	afficheur.mapAffichage("    ");
	delay(100);
	changeState(IDLE);
}
void changeState(MinuteurState mstate) {
	markChange();
	state = mstate;
	debug("Etat:");
	debugln(state);
}

void recordLastRead(int min, int sec) {
	//debug("Lecture : ");
	//debug(min);
	//debug(":");
	//debugln(sec);
	if (abs(min-lastMin) >= 1 || abs(sec-lastSec) >= 1) {
		markChange();
	}
	lastMin = min;
	lastSec = sec;
}

void pin2Interrupt(void) {
	/* This will bring us back from sleep. */

	/* We detach the interrupt to stop it from
	 * continuously firing while the interrupt pin
	 * is low.
	 */
	detachInterrupt(0);
}

Melody chooseMusic(Melody* melodies, int* weight, int tab_size) {
	int total_weight=0;
	for (int i=0; i<tab_size;i++){
		total_weight+=weight[i];
	}
	int spot=millis()%total_weight;
	for(int i=0; i<tab_size;i++){
		if (spot<weight[i]){
			return melodies[i];
		}
		spot=spot-weight[i];
	}
	return melodies[0];
}
